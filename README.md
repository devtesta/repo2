# Generated Documentation
## test_dind
Description: your role description
For details on how to contribute, please read the [Contributing Guidelines](https://gitlab.com/CONTRIBUTING.md).

# Table of Contents

- [Introduction](#introduction)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
  - [Basic Usage](#basic-usage)
  - [Advanced Usage](#advanced-usage)
- [Contributing](#contributing)
- [License](#license)
- [Acknowledgments](#acknowledgments)


For details on how to contribute, please read the:
- [x] [Contributing Guidelines](https://gitlab.com/CONTRIBUTING.md).
- [ ] [Contributing Guidelines](https://gitlab.com/CONTRIBUTING.md).
- [ ] [Contributing Guidelines](https://gitlab.com/CONTRIBUTING.md).

## Introduction

This is the introduction section. Here we briefly discuss what this project is about.

## Getting Started

These instructions will help you get started with the project.

### Prerequisites

What you need to install before running the project.

### Installation

Step-by-step guide to install the project.

## Usage

How to use the project.

### Basic Usage

Some basic usage examples.

### Advanced Usage

Some advanced usage examples.

## Contributing

How to contribute to the project.

## License

Information about the license.

## Acknowledgments

Thanking people and resources that helped with this project.

### Flags

- `--role`: Specifies the directory path to the Ansible role.
- `--playbook`: Specifies the path to the Ansible playbook (Optional).
- `--graph`: Generate mermaid for role and playbook.
- `--no-backup`: Ignore existent README.md and remove before generate a new one. (Optional).

## Features
- Generates a README in Markdown format
- Scans and includes default variables and role-specific variables
- Parses tasks, including special Ansible task types like 'block' and 'rescue'
- Optionally includes playbook content in the README
- CLI-based, easy to integrate into a CI/CD pipeline
- Provides a templating feature to customize output
- Supports multiple YAML files within `tasks`, `defaults`, `vars` directory
- Includes meta-data like author and license from `meta/main.yml`
- Generates a well-structured table for default and role-specific variables
- Support for encrypted Ansible Vault variables

| Field                | Value           |
|--------------------- |-----------------|
| Functional description | Not available. |
| Requester            | Not available. |
| Users                | Not available. |
| Date dev             | Not available. |
| Date prod            | Not available. |
| Version              | Not available. |
| Time Saving              | Not available. |

test `var` test

### Defaults
**These are static variables with lower priority**
#### File: main.yml
| Var          | Type         | Value       | Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| var    | str   | var1  | True  | variable sert a faire ca.. |
| var2    | str   | 2  | True  | variable 2 sert a faire ca.. |
| var3    | str   | plm  | True  | variable 3 sert a faire ca.. |
#### File: na.yml
| Var          | Type         | Value       | Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| `var`    | str   | var1  | True  | variable sert a faire ca.. |
| `var2`    | str   | 2  | True  | variable 2 sert a faire ca.. |
| `var3`    | str   | plm  | True  | variable 3 sert a faire ca.. |
| your_variable_name    | str   | ENCRYPTED_WITH_ANSIBLE_VAULT  | True  | variable chiffré |



### Vars
**These are variables with higher priority**
#### File: main.yml
| Var          | Type         | Value       | Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| var    | str   | var1  | True  | variable sert a faire ca.. |
| var2    | str   | 2  | True  | variable 2 sert a faire ca.. |
| var3    | str   | plm  | True  | variable 3 sert a faire ca.. |
#### File: plm.yml
| Var          | Type         | Value       | Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| var    | str   | var1  | True  | variable sert a faire ca.. |
| var2    | str   | 2  | True  | variable 2 sert a faire ca.. |
| var3    | str   | plm  | True  | variable 3 sert a faire ca.. |


### Tasks
| Name | Module | Condition |
| ---- | ------ | --------- |
| Initial setup | ansible.builtin.debug | N/A |
| Initial block setup | block | N/A |
| Block 1 inside block setup, dskgjhfkjgksgnklsdghs g sfdg fsd gfd h dsf h g h dfsgj dfhsdfgsdfhsdfh sd h sdf h sdf h sghd sfd gsdfg | block | ['decision_point_1 == "go" \\| decision_point_1 == "da" \\| decision_point_1 == "go" \\| decision_point_1 == "da"', 'decision_point_1 == "go" \\| decision_point_1 == "da" \\| decision_point_1 == "go" \\| decision_point_1 == "da"'] |
| Decision point 1 - Go | ansible.builtin.debug | N/A |
| Further action based on decision 1 | ansible.builtin.debug | N/A |
| Block 2 inside block setup | block | decision_point_1 != "go" |
| Decision point 3 - Stop | ansible.builtin.debug | N/A |
| Further action based on decision 1 | ansible.builtin.debug | N/A |
| Block 2 inside block setup | block | decision_point_2 == "proceed" |
| Decision point 4 - Proceed | ansible.builtin.debug | N/A |
| Further action based on decision 2 | ansible.builtin.debug | N/A |
| Unnamed_block | block | decision_point_2 != "proceed" |
| Decision point 2 - Rollback | ansible.builtin.debug | N/A |
| Further action based on decision 2 | ansible.builtin.debug | N/A |
| Unnamed_block | block | N/A |
| Final action if everything is okay | ansible.builtin.debug | N/A |


## Task Flow Graphs

### Graph for main.yml
```mermaid
flowchart TD
  Start
  Start-->|Task| Initial_setup[initial setup]
  Initial_setup-.->|End of Task| Initial_setup
  Initial_setup-->|Block Start| Initial_block_setup_block_start_0[initial block setup]
  Initial_block_setup_block_start_0-->|"Block Start (When: decision point 1     go    decision point 1     da<br>   decision point 1     go    decision point 1    <br>da  and decision point 1     go    decision point<br>1     da    decision point 1     go    decision<br>point 1     da )"| Block_1_inside_block_setup__dskgjhfkjgksgnklsdghs_g_sfdg_fsd_gfd_h_dsf_h_g_h_dfsgj_dfhsdfgsdfhsdfh_sd_h_sdf_h_sdf_h_sghd_sfd_gsdfg_when_decision_point_1_____go____decision_point_1_____da____decision_point_1_____go____decision_point_1_____da__AND_decision_point_1_____go____decision_point_1_____da____decision_point_1_____go____decision_point_1_____da__block_start_1[block 1 inside block setup  dskgjhfkjgksgnklsdghs<br>g sfdg fsd gfd h dsf h g h dfsgj dfhsdfgsdfhsdfh<br>sd h sdf h sdf h sghd sfd gsdfg]
  Block_1_inside_block_setup__dskgjhfkjgksgnklsdghs_g_sfdg_fsd_gfd_h_dsf_h_g_h_dfsgj_dfhsdfgsdfhsdfh_sd_h_sdf_h_sdf_h_sghd_sfd_gsdfg_when_decision_point_1_____go____decision_point_1_____da____decision_point_1_____go____decision_point_1_____da__AND_decision_point_1_____go____decision_point_1_____da____decision_point_1_____go____decision_point_1_____da__block_start_1-->|Task| Decision_point_1___Go[decision point 1   go]
  Decision_point_1___Go-->|Task| Further_action_based_on_decision_1[further action based on decision 1]
  Further_action_based_on_decision_1-.->|End of Block| Block_1_inside_block_setup__dskgjhfkjgksgnklsdghs_g_sfdg_fsd_gfd_h_dsf_h_g_h_dfsgj_dfhsdfgsdfhsdfh_sd_h_sdf_h_sdf_h_sghd_sfd_gsdfg_when_decision_point_1_____go____decision_point_1_____da____decision_point_1_____go____decision_point_1_____da__AND_decision_point_1_____go____decision_point_1_____da____decision_point_1_____go____decision_point_1_____da__block_start_1
  Further_action_based_on_decision_1-->|"Block Start (When: decision point 1     go )"| Block_2_inside_block_setup_when_decision_point_1_____go__block_start_1[block 2 inside block setup]
  Block_2_inside_block_setup_when_decision_point_1_____go__block_start_1-->|Task| Decision_point_3___Stop[decision point 3   stop]
  Decision_point_3___Stop-->|Task| Further_action_based_on_decision_1[further action based on decision 1]
  Further_action_based_on_decision_1-.->|End of Block| Block_2_inside_block_setup_when_decision_point_1_____go__block_start_1
  Further_action_based_on_decision_1-->|"Block Start (When: decision point 2     proceed )"| Block_2_inside_block_setup_when_decision_point_2_____proceed__block_start_1[block 2 inside block setup]
  Block_2_inside_block_setup_when_decision_point_2_____proceed__block_start_1-->|Task| Decision_point_4___Proceed[decision point 4   proceed]
  Decision_point_4___Proceed-->|Task| Further_action_based_on_decision_2[further action based on decision 2]
  Further_action_based_on_decision_2-.->|End of Block| Block_2_inside_block_setup_when_decision_point_2_____proceed__block_start_1
  Further_action_based_on_decision_2-->|"Block Start (When: decision point 2     proceed )"| Unnamed_task_3_when_decision_point_2_____proceed__block_start_1[unnamed task 3]
  Unnamed_task_3_when_decision_point_2_____proceed__block_start_1-->|Task| Decision_point_2___Rollback[decision point 2   rollback]
  Decision_point_2___Rollback-->|Task| Further_action_based_on_decision_2[further action based on decision 2]
  Further_action_based_on_decision_2-.->|End of Block| Unnamed_task_3_when_decision_point_2_____proceed__block_start_1
  Further_action_based_on_decision_2-->|Block Start| Unnamed_task_4_block_start_1[unnamed task 4]
  Unnamed_task_4_block_start_1-->|Task| Final_action_if_everything_is_okay[final action if everything is okay]
  Final_action_if_everything_is_okay-.->|End of Block| Unnamed_task_4_block_start_1
  Final_action_if_everything_is_okay-->|Rescue Start| Unnamed_task_4_rescue_start_1[unnamed task 4]
  Unnamed_task_4_rescue_start_1-->|Task| Error_Handling[error handling]
  Error_Handling-.->|End of Rescue Block| Unnamed_task_4_block_start_1
  Error_Handling-->|Rescue Start| Initial_block_setup_rescue_start_0[initial block setup]
  Initial_block_setup_rescue_start_0-->|Task| Error_Handling[error handling]
  Error_Handling-.->|End of Rescue Block| Initial_block_setup_block_start_0
```


## Playbook
```yml
---
- name: Sample Ansible Playbook with Multiple Features
  hosts: localhost
  roles:
    - role1
    - role2

  tasks:
    - name: This is a simple task
      debug:
        msg: "This is a simple debug message"

    - name: This is a block with rescue
      block:
        - name: Task within a block that will fail
          command: /bin/false
        - name: Another task within a block 1
          debug:
            msg: "This task will not run because the first task within the block will fail"
      rescue:
        - name: This will be executed if the block fails
          debug:
            msg: "Block failed, now in rescue"

    - name: This is a block without rescue
      block:
        - name: Task within a block that will succeed
          command: /bin/true
        - name: Another task within a block 2
          debug:
            msg: "This task will run because the first task within the block will succeed"

    - name: dbg
      debug:
        var: res_docker_compose


    - name: This is a simple task 3
      debug:
        msg: "This is a simple task in role"

    - name: This is a block with rescue 3
      block:

        - name: Task 1 within a block that will fail 2
          command: /bin/false

        - name: Task 2 within a block 2
          debug:
            msg: "This task will not run because the first task within the block will fail"

      rescue:

        - name: Rescue task for block 19
          debug:
            msg: "Block failed, executing tasks in rescue"

    - name: This is another block without rescue 7897
      block:

        - name: Task 1 within a block that will succeed 987
          command: /bin/true

        - name: Task 2 within a block 7565
          debug:
            msg: "This task will run because the first task within the block will succeed"

    - name: This is a simple task 5
      debug:
        msg: "This is a simple debug message"
```
## Playbook graph
```mermaid
flowchart TD
  localhost-->|Role| role1[role1]
  role1-->|Role| role2[role2]
  role2-->|Task| This_is_a_simple_task[this is a simple task]
  This_is_a_simple_task-.->|End of Task| This_is_a_simple_task
  This_is_a_simple_task-->|Block Start| This_is_a_block_with_rescue_block_start_0[this is a block with rescue]
  This_is_a_block_with_rescue_block_start_0-->|Task| Task_within_a_block_that_will_fail[task within a block that will fail]
  Task_within_a_block_that_will_fail-->|Task| Another_task_within_a_block_1[another task within a block 1]
  Another_task_within_a_block_1-.->|End of Block| This_is_a_block_with_rescue_block_start_0
  Another_task_within_a_block_1-->|Rescue Start| This_is_a_block_with_rescue_rescue_start_0[this is a block with rescue]
  This_is_a_block_with_rescue_rescue_start_0-->|Task| This_will_be_executed_if_the_block_fails[this will be executed if the block fails]
  This_will_be_executed_if_the_block_fails-.->|End of Rescue Block| This_is_a_block_with_rescue_block_start_0
  This_will_be_executed_if_the_block_fails-->|Block Start| This_is_a_block_without_rescue_block_start_0[this is a block without rescue]
  This_is_a_block_without_rescue_block_start_0-->|Task| Task_within_a_block_that_will_succeed[task within a block that will succeed]
  Task_within_a_block_that_will_succeed-->|Task| Another_task_within_a_block_2[another task within a block 2]
  Another_task_within_a_block_2-.->|End of Block| This_is_a_block_without_rescue_block_start_0
  Another_task_within_a_block_2-->|Task| dbg[dbg]
  dbg-.->|End of Task| dbg
  dbg-->|Task| This_is_a_simple_task_3[this is a simple task 3]
  This_is_a_simple_task_3-.->|End of Task| This_is_a_simple_task_3
  This_is_a_simple_task_3-->|Block Start| This_is_a_block_with_rescue_3_block_start_0[this is a block with rescue 3]
  This_is_a_block_with_rescue_3_block_start_0-->|Task| Task_1_within_a_block_that_will_fail_2[task 1 within a block that will fail 2]
  Task_1_within_a_block_that_will_fail_2-->|Task| Task_2_within_a_block_2[task 2 within a block 2]
  Task_2_within_a_block_2-.->|End of Block| This_is_a_block_with_rescue_3_block_start_0
  Task_2_within_a_block_2-->|Rescue Start| This_is_a_block_with_rescue_3_rescue_start_0[this is a block with rescue 3]
  This_is_a_block_with_rescue_3_rescue_start_0-->|Task| Rescue_task_for_block_19[rescue task for block 19]
  Rescue_task_for_block_19-.->|End of Rescue Block| This_is_a_block_with_rescue_3_block_start_0
  Rescue_task_for_block_19-->|Block Start| This_is_another_block_without_rescue_7897_block_start_0[this is another block without rescue 7897]
  This_is_another_block_without_rescue_7897_block_start_0-->|Task| Task_1_within_a_block_that_will_succeed_987[task 1 within a block that will succeed 987]
  Task_1_within_a_block_that_will_succeed_987-->|Task| Task_2_within_a_block_7565[task 2 within a block 7565]
  Task_2_within_a_block_7565-.->|End of Block| This_is_another_block_without_rescue_7897_block_start_0
  Task_2_within_a_block_7565-->|Task| This_is_a_simple_task_5[this is a simple task 5]
  This_is_a_simple_task_5-.->|End of Task| This_is_a_simple_task_5
```

## Author Information
Lucian

#### License
license (GPL-2.0-or-later, MIT, etc)

#### Minimum Ansible Version
2.1

#### Platforms
No platforms specified.